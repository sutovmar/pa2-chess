#include "Game.hpp"

//======================================================================================================================
void Game::InitWhite() {

    Rook * r1 = new Rook(1,1, WHITE);
    this->white.figures.emplace("r1", r1);
    this->white.player_layout.emplace(std::make_pair(1,1), r1);

    Rook * r2 = new Rook(8,1, WHITE);
    this->white.figures.emplace("r2", r2);
    this->white.player_layout.emplace(std::make_pair(8,1), r2);

//    Knight * k1 = new Knight(2,1, WHITE);
//    this->white.figures.emplace("k1",k1);
//    this->white.player_layout.emplace(std::make_pair(2,1),k1);
//
//    Knight * k2 = new Knight(7,1, WHITE);
//    this->white.figures.emplace("k2",k2);
//    this->white.player_layout.emplace(std::make_pair(7,1),k2);
//
//    Bishop * b1 = new Bishop(3,1, WHITE);
//    this->white.figures.emplace("b1",b1);
//    this->white.player_layout.emplace(std::make_pair(3,1),b1);
//
//    Bishop * b2 = new Bishop(6,1, WHITE);
//    this->white.figures.emplace("b2",b2);
//    this->white.player_layout.emplace(std::make_pair(6,1),b2);

//    Queen * q = new Queen(4,1, WHITE);
//    this->white.figures.emplace("q", q);
//    this->white.player_layout.emplace(std::make_pair(4,1), q);

    King * k = new King(5,1, WHITE);
    this->white.figures.emplace("k",k);
    this->white.player_layout.emplace(std::make_pair(5,1), k);

    Pawn * p1 = new Pawn(1,2, WHITE);
    Pawn * p2 = new Pawn(2,2, WHITE);
    Pawn * p3 = new Pawn(3,2, WHITE);
    Pawn * p4 = new Pawn(4,2, WHITE);
    Pawn * p5 = new Pawn(5,2, WHITE);
    Pawn * p6 = new Pawn(6,2, WHITE);
    Pawn * p7 = new Pawn(7,2, WHITE);
    Pawn * p8 = new Pawn(8,5, WHITE);


    this->white.figures.emplace("p1",p1);
    this->white.player_layout.emplace(std::make_pair(1,2), p1);

    this->white.figures.emplace("p2",p2);
    this->white.player_layout.emplace(std::make_pair(2,2), p2);

    this->white.figures.emplace("p3",p3);
    this->white.player_layout.emplace(std::make_pair(3,2), p3);

    this->white.figures.emplace("p4",p4);
    this->white.player_layout.emplace(std::make_pair(4,2), p4);

    this->white.figures.emplace("p5",p5);
    this->white.player_layout.emplace(std::make_pair(5,2), p5);

    this->white.figures.emplace("p6",p6);
    this->white.player_layout.emplace(std::make_pair(6,2), p6);

    this->white.figures.emplace("p7",p7);
    this->white.player_layout.emplace(std::make_pair(7,2), p7);

    this->white.figures.emplace("p8",p8);
    this->white.player_layout.emplace(std::make_pair(8,5), p8);
}
//======================================================================================================================
void Game::InitBlack() {

    Rook * r1 = new Rook(1,8, BLACK);
    this->black.figures.emplace("r1", r1);
    this->black.player_layout.emplace(std::make_pair(1,8), r1);

    Rook * r2 = new Rook(8,8, BLACK);
    this->black.figures.emplace("r2", r2);
    this->black.player_layout.emplace(std::make_pair(8,8), r2);

//    Knight * k1 = new Knight(2,8, BLACK);
//    this->black.figures.emplace("k1",k1);
//    this->black.player_layout.emplace(std::make_pair(2,8),k1);
//
//    Knight * k2 = new Knight(7,8, BLACK);
//    this->black.figures.emplace("k2",k2);
//    this->black.player_layout.emplace(std::make_pair(7,8),k2);
//
//    Bishop * b1 = new Bishop(3,8, BLACK);
//    this->black.figures.emplace("b1",b1);
//    this->black.player_layout.emplace(std::make_pair(3,8),b1);
//
//    Bishop * b2 = new Bishop(6,8, BLACK);
//    this->black.figures.emplace("b2",b2);
//    this->black.player_layout.emplace(std::make_pair(6,8),b2);

//    Queen * q = new Queen(4,8, BLACK);
//    this->black.figures.emplace("q", q);
//    this->black.player_layout.emplace(std::make_pair(4,8), q);

    King * k = new King(5,8, BLACK);
    this->black.figures.emplace("k",k);
    this->black.player_layout.emplace(std::make_pair(5,8), k);


    Pawn * p1 = new Pawn(1,7, BLACK);
    Pawn * p2 = new Pawn(2,7, BLACK);
    Pawn * p3 = new Pawn(3,7, BLACK);
    Pawn * p4 = new Pawn(4,7, BLACK);
    Pawn * p5 = new Pawn(5,7, BLACK);
    Pawn * p6 = new Pawn(6,4, BLACK);
    Pawn * p7 = new Pawn(7,7, BLACK);
//    Pawn * p8 = new Pawn(8,7, BLACK);


    this->black.figures.emplace("p1",p1);
    this->black.player_layout.emplace(std::make_pair(1,7), p1);

    this->black.figures.emplace("p2",p2);
    this->black.player_layout.emplace(std::make_pair(2,7), p2);

    this->black.figures.emplace("p3",p3);
    this->black.player_layout.emplace(std::make_pair(3,7), p3);

    this->black.figures.emplace("p4",p4);
    this->black.player_layout.emplace(std::make_pair(4,7), p4);

    this->black.figures.emplace("p5",p5);
    this->black.player_layout.emplace(std::make_pair(5,7), p5);

    this->black.figures.emplace("p6",p6);
    this->black.player_layout.emplace(std::make_pair(6,4), p6);

    this->black.figures.emplace("p7",p7);
    this->black.player_layout.emplace(std::make_pair(7,7), p7);

//    this->black.figures.emplace("p8",p8);
//    this->black.player_layout.emplace(std::make_pair(8,7), p8);
}
//======================================================================================================================
Game::~Game() {

    for( auto i : this->white.figures )
    {
        delete i.second;
    }

    for( auto i : this->black.figures )
    {
        delete i.second;
    }
}
//======================================================================================================================
bool Game::isCheckMate( const COLOR & color ) {

    Figure *                                king;
    std::set<std::pair<unsigned, unsigned>> available_options;
    std::set<std::pair<unsigned, unsigned>> unsafe_positions;
    std::map<std::string, Figure*>          threatening_figures;
    std::pair<unsigned, unsigned>           king_position;

    if ( color == WHITE )
    {
        king = this->black.figures.find("k")->second;

        threatening_figures = this->white.figures;

    } else {

        king = this->white.figures.find("k")->second;

        threatening_figures = this->black.figures;
    }

    king_position = king->getCoordinates();

    if(     ( ( king_position.first + 1 ) <= 8 && ( king_position.first + 1 ) >= 1 )
            &&  ( ( king_position.second + 1 ) <= 8 && ( king_position.second + 1 ) >= 1 )
            &&  ( this->white.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second + 1) ) == this->white.player_layout.end() )
            &&  ( this->black.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second + 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first + 1, king_position.second + 1));
    }

    // [ 1 , 0 ]
    if(         ( ( king_position.first + 1 ) <= 8 && ( king_position.first + 1 ) >= 1 )
                &&  ( ( king_position.second  ) <= 8 && ( king_position.second ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second ) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second ) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first + 1, king_position.second ));
    }

    // [ 1 , -1 ]
    if(         ( ( king_position.first + 1 ) <= 8 && ( king_position.first + 1 ) >= 1 )
                &&  ( ( king_position.second - 1 ) <= 8 && ( king_position.second - 1 ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second - 1) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second - 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first + 1, king_position.second - 1));
    }

    // [ 0 , -1 ]
    if(         ( ( king_position.first ) <= 8 && ( king_position.first  ) >= 1 )
                &&  ( ( king_position.second - 1 ) <= 8 && ( king_position.second - 1 ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first  , king_position.second - 1) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first  , king_position.second - 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first, king_position.second - 1));
    }

    // [ 1 , 1 ]
    if(         ( ( king_position.first + 1 ) <= 8 && ( king_position.first + 1 ) >= 1 )
                &&  ( ( king_position.second + 1 ) <= 8 && ( king_position.second + 1 ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second + 1) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first + 1 , king_position.second + 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first + 1, king_position.second + 1));
    }

    // [ -1 , -1 ]
    if(         ( ( king_position.first - 1 ) <= 8 && ( king_position.first - 1 ) >= 1 )
                &&  ( ( king_position.second - 1 ) <= 8 && ( king_position.second - 1 ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second - 1) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second - 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first - 1, king_position.second - 1));
    }

    // [ -1 , 0 ]
    if(         ( ( king_position.first - 1 ) <= 8 && ( king_position.first - 1 ) >= 1 )
                &&  ( ( king_position.second  ) <= 8 && ( king_position.second  ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second ) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second ) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first - 1, king_position.second ));
    }

    // [ -1 , 1 ]
    if(         ( ( king_position.first - 1 ) <= 8 && ( king_position.first - 1 ) >= 1 )
                &&  ( ( king_position.second + 1 ) <= 8 && ( king_position.second + 1 ) >= 1 )
                &&  ( this->white.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second + 1) ) == this->white.player_layout.end() )
                &&  ( this->black.player_layout.find(std::make_pair(king_position.first - 1 , king_position.second + 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first - 1, king_position.second + 1));
    }

    // [ 0 , 1 ]
    if(     ( ( king_position.first  ) <= 8 && ( king_position.first  ) >= 1 )
            &&  ( ( king_position.second + 1 ) <= 8 && ( king_position.second + 1 ) >= 1 )
            &&  ( this->white.player_layout.find(std::make_pair(king_position.first  , king_position.second + 1) ) == this->white.player_layout.end() )
            &&  ( this->black.player_layout.find(std::make_pair(king_position.first  , king_position.second + 1) ) == this->black.player_layout.end() )
            )
    {
        available_options.emplace(std::make_pair( king_position.first , king_position.second + 1));
    }

    for( auto i :  threatening_figures )
    {
        std::pair<unsigned, unsigned> figure_positions = i.second->getCoordinates();

        for( auto p: available_options )
        {
            if( ((this->checkRoute(figure_positions.first, figure_positions.second, p.first, p.second)) && i.second->threatenPosition(p.first, p.second)) )
            {
                if ( unsafe_positions.find(std::make_pair(p.first, p.second)) == unsafe_positions.end() )
                {
                    unsafe_positions.emplace(std::make_pair(p.first, p.second));
                }

            }

        }
    }

    if( available_options.size() != 0 )
    {
        if( unsafe_positions.size() ==  available_options.size() )
        {
            std::cout << color << " WON - checkmate" << std::endl;

            return true;
        }
    }

    return false;


}
//======================================================================================================================
bool Game::isCheck(const COLOR &c) {

    Figure* king;
    std::map<std::string, Figure*> threatening_figures;

    if ( c == WHITE )
    {
        king = this->white.figures.find("k")->second;
        threatening_figures = this->black.figures;

    } else {
        king = this->black.figures.find("k")->second;
        threatening_figures = this->white.figures;
    }

    std::pair<unsigned, unsigned> position = (king)->getCoordinates();

    for( auto figure : threatening_figures )
    {
        if ( (figure.second)->threatenPosition( position.first, position.second) )
        {

            return true;
        }
    }

    return false;
}
//======================================================================================================================
int Game::parseCommand(std::string &command, const COLOR & color) {

    std::locale loc;
    std::string lower_command;

    for( auto c : command )
    {
        lower_command=lower_command+std::tolower(c,loc);
    }

    if ( lower_command == "exit")
    {
        std::cout << "Exiting game " << std::endl;
        return 0;
    }

    if ( lower_command == "save")
    {
        std::cout << "Saving game " << std::endl;

        if( !this->SaveGame())
        {
            std::cout << "Error while saving file" << std::endl;
        }

        return 2;
    }

    if ( lower_command == "move")
    {
        std::cout << "Make your move " << std::endl;

        std::string figure, x_position;
        unsigned y_position;

        std::cin.clear();
        std::cin >> figure >> x_position >> y_position;

        while ( std::cin.fail() || !this->validMove(color, figure, x_position, y_position,  true, true) )
        {
            std::cout << "Wrong input" << std::endl;

            std::cin.clear();
            std::cin.ignore(1024, '\n');

            std::cin >> figure >> x_position >> y_position;
        }

        if( this->isCheckMate(color))
        {
            std::cout << "CHECKMATE" << std::endl;
            return 1;
        }

        if( this->isCheck(color) )
        {
            if( color == WHITE)
                std::cout << "Black king is in check" << std::endl;
            else
                std::cout << "White king is in check" << std::endl;
        }

        return 1;
    }

    return -1;

}
//======================================================================================================================
bool Game::validMove(COLOR color, std::string figure, std::string x, unsigned y, bool action, bool log ) {

    unsigned x_position = this->parseXPosition(x);
    unsigned y_position = y;

    std::map<std::string, Figure*>                      friendly_figures;
    std::map<std::pair<unsigned ,unsigned>, Figure*>    friendly_layout;

    std::map<std::string, Figure*>                      enemy_figures;
    std::map<std::pair<unsigned ,unsigned>, Figure*>    enemy_layout;

    unsigned pawn_promotion;
    int en_pasant_position;

    if ( color == WHITE )
    {
        friendly_figures = this->white.figures;
        friendly_layout  = this->white.player_layout;

        enemy_figures = this->black.figures;
        enemy_layout  = this->black.player_layout;

        pawn_promotion = 8;
        en_pasant_position = -1;

    } else {

        friendly_figures = this->black.figures;
        friendly_layout  = this->black.player_layout;

        enemy_figures = this->white.figures;
        enemy_layout  = this->white.player_layout;

        pawn_promotion = 1;

        en_pasant_position = 1;
    }

    std::map<std::string, Figure *>::iterator f = friendly_figures.find(figure);

    if ( f == friendly_figures.end() )
    {
        if ( log )
            std::cout << "Figure not found" << std::endl;
        return 0;

    }

    std::pair<unsigned, unsigned> figure_position = (f->second)->getCoordinates();

    if( !(f->second)->isKnight() )
    {
        if( !( this->checkRoute(figure_position.first, figure_position.second, x_position, y_position ) ) )
        {
            if( log )
                std::cout << "Something in the way" << std::endl;
            return 0;
        }
    }

    if( (f->second)->isKing() )
    {
        if( this->validCastling(x_position,y_position, action) )
        {
            std::cout << "Valid castling" << std::endl;
            return 1;
        }
    }

    if ((f->second)->isPawn() )
    {
        if( y_position == pawn_promotion )
        {
            if(!f->second->setPosition(x_position,y_position))
            {
                if( log )
                    std::cout << "Invalid move " << std::endl;
                return 0;
            }

            if(this->isCheck(color) )
            {

                f->second->undoMove();
                if( log )
                    std::cout << "Move makes king vunerable" << std::endl;
                return 0;
            }

            f->second->undoMove();

            if( action ) {

                this->promotePawn(*(f->second), color);
            }

            return 1;
        }

        if( abs((int)(figure_position.first) - (int)(x_position)) == 1 && (figure_position.second - (en_pasant_position) ) == y_position )
        {
            std::map<std::pair<unsigned, unsigned>, Figure*>::iterator enemy_figure = enemy_layout.find(std::make_pair(x_position, y_position));

            if ( enemy_figure != enemy_layout.end() && action )
            {
                (enemy_figure->second)->killFigure();

                if( color == WHITE)
                {
                    this->black.player_layout.erase(std::make_pair(x_position, y_position));

                } else {
                    this->white.player_layout.erase(std::make_pair(x_position, y_position));
                }

            } else {

                std::map<std::pair<unsigned, unsigned>, Figure*>::iterator en_pasant_figure = enemy_layout.find(std::make_pair(x_position, y_position+en_pasant_position));

                if( en_pasant_figure != enemy_layout.end() && (en_pasant_figure->second)->isPawn() )
                {
                    Pawn * p = dynamic_cast<Pawn*>(en_pasant_figure->second);
                    std::pair<unsigned, unsigned> pawn_position = p->getCoordinates();

                    if( pawn_position.first == x_position && pawn_position.second == y_position+1 && p->wasLastMoveDouble() )
                    {
                        if( action )
                        {
                            (en_pasant_figure->second)->killFigure();

                            if( color == WHITE )
                                this->black.player_layout.erase(std::make_pair(x_position, y_position+en_pasant_position));
                            else
                                this->white.player_layout.erase(std::make_pair(x_position, y_position+en_pasant_position));

                        }
                    } else {

                        if( log )
                            std::cout << "Invalid en pasant" << std::endl;

                        return 0;
                    }

                } else {
                    if( log )
                        std::cout << "Invalid en pasant" << std::endl;
                    return 0;
                }

            }

        } else {

            auto black_figure = this->black.player_layout.find(std::make_pair(x_position, y_position));

            auto white_figure = this->white.player_layout.find(std::make_pair(x_position, y_position));

            if( black_figure != this->black.player_layout.end() || white_figure != this->white.player_layout.end() )
            {
                if (log)
                    std::cout << "Pawn can't move, something in the way" << std::endl;

                return 0;

            }
        }

        if( !(f->second)->setPosition(x_position, y_position))
        {
            if ( log )
                std::cout << "Invalid move " << std::endl;
            return 0;
        }

        if( this->isCheck(color) )
        {
            f->second->undoMove();

            if ( log )
                std::cout << "This move makes king vunerable" << std::endl;

            return 0;
        }

    } else if ( !( this->checkDestination( x_position, y_position, color ,action ) )||  !((f->second)->setPosition(x_position,y_position)))
    {
        if ( log )
            std::cout << "Invalid move " << std::endl;
        return 0;
    }

    if( this->isCheck(color) )
    {

        (f->second)->undoMove();
        if(log)
            std::cout << "This move makes king vunerable" << std::endl;
        return 0;
    }

    if( action )
    {
        if( color == WHITE )
        {
            this->white.player_layout.erase(figure_position);
            this->white.player_layout.emplace(std::make_pair(x_position, y_position), f->second);

        } else {

            this->black.player_layout.erase(figure_position);
            this->black.player_layout.emplace(std::make_pair(x_position, y_position), f->second);
        }
    }


    return 1;

}
//======================================================================================================================
bool Game::SaveGame() {

    srand(time(0));

    std::string name = "chess-" + std::to_string(rand()) + ".txt";
    const char * file_name = name.c_str();

    std::ofstream save_file( file_name, std::ios::out);

    if( save_file.fail() || !(save_file.is_open()) || !(save_file.good()) )
    {
        save_file.close();
        return false;
    }

    for( auto figure : this->white.figures )
    {
        save_file << figure.first << " " <<  (figure.second)->getName() << " " <<  (figure.second)->saveFigure() << " |";

        if( save_file.fail() )
        {
            save_file.close();
            return false;
        }
    }

    save_file << "\n";

    for( auto figure : this->black.figures )
    {
        save_file << figure.first << " " <<  (figure.second)->getName() << " " <<  (figure.second)->saveFigure() << " |";

        if( save_file.fail() )
        {
            save_file.close();
            return false;
        }
    }

    save_file.close();

    return 1;


}
//======================================================================================================================
bool Game::LoadGame(const char * filename) {

    std::ifstream input_file( filename, std::ios::out);

    if( input_file.fail() || !(input_file.is_open()) || !(input_file.good()) )
    {
        input_file.close();
        return false;
    }

    std::string white;
    std::string black;

    std::getline(input_file, white,'\n');

    if( input_file.fail() || input_file.bad() )
    {
        input_file.close();
        return false;
    }

    std::getline(input_file, black,'\n');

    if( input_file.fail() || input_file.bad() )
    {
        input_file.close();
        return false;
    }

    std::istringstream white_stream(white);
    std::string input;

    while( getline(white_stream, input, '|') )
    {
        if( !this->LoadFigure(input, WHITE))
        {
            return false;
        }

    }

    std::istringstream black_stream(black);
    input="";

    while( getline(black_stream, input, '|') )
    {
        if( !this->LoadFigure(input, BLACK))
        {
            return false;
        }

    }

    return true;
}
//======================================================================================================================
void Game::startGame() {

    std::string game_start;

    std::cout << "New - [START NEW GAME] | Load - [ LOAD GAME FROM FILE ]" << std::endl;

    std::cin.clear();

    std::cin >> game_start;

    std::locale loc;
    std::string lower_command;

    for( auto c : game_start )
    {
        lower_command=lower_command+std::tolower(c,loc);
    }

    while ( lower_command != "new" &&  lower_command != "load")
    {
        std::cout << "Wrong input" << std::endl;
        std::cin.clear();
        std::cin.ignore(1024, '\n');

        lower_command = "";
        std::cin >> game_start;

        for( auto c : game_start )
        {
            lower_command=lower_command+std::tolower(c,loc);
        }
    }

    if( lower_command == "new" )
    {
        this->InitWhite();
        this->InitBlack();

    } else {

        std::cout << "Enter file name: " << std::endl;
        std::cin.clear();
        std::string file_name;

        std::cin >> file_name;

        if ( !this->LoadGame(file_name.c_str()) )
        {
            std::cout << "Error while loading file" << std::endl;
            this->startGame();
        }

    }


}
//======================================================================================================================
bool Game::LoadFigure(std::string figure, const COLOR &c) {

    std::istringstream figure_stream(figure);

    std::string figure_name;
    std::string saved_name;

    getline(figure_stream, saved_name, ' ');
    getline(figure_stream, figure_name, ' ');

    std::string figure_x;
    std::string figure_y;
    std::string figure_alive;
    std::string figure_last_x;
    std::string figure_last_y;

    getline(figure_stream, figure_x, ' ');
    getline(figure_stream, figure_y, ' ');
    getline(figure_stream, figure_alive, ' ');
    getline(figure_stream, figure_last_x, ' ');
    getline(figure_stream, figure_last_y, ' ');

    if ( figure_name == "PAWN" )
    {
        std::string figure_first_move;
        std::string figure_double_move;
        std::string figure_prev_first_move;
        std::string figure_prev_double_move;

        getline(figure_stream, figure_first_move, ' ');
        getline(figure_stream, figure_double_move, ' ');
        getline(figure_stream, figure_prev_first_move, ' ');
        getline(figure_stream, figure_prev_double_move, ' ');


        Pawn * p = new Pawn( c,
                             std::stoul(figure_y),
                             std::stoul(figure_x),
                             std::stoul(figure_last_x),
                             std::stoul(figure_last_y),
                             (figure_alive == "1")?1:0 ,
                             (figure_double_move == "1")?1:0,
                             (figure_first_move == "1")?1:0,
                             (figure_prev_double_move == "1")?1:0,
                             (figure_prev_first_move == "1")?1:0);


        if( c == WHITE)
        {
            this->white.figures.emplace(saved_name,p);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), p);
        } else {
            this->black.figures.emplace(saved_name,p);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), p);
        }


    } else if( figure_name == "QUEEN" )
    {
        Queen * q= new Queen( c, std::stoul(figure_y),
                              std::stoul(figure_x),
                              std::stoul(figure_last_x),
                              std::stoul(figure_last_y),
                              (figure_alive == "1")?1:0 );

        if( c == WHITE)
        {
            this->white.figures.emplace(saved_name,q);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), q);
        } else {
            this->black.figures.emplace(saved_name,q);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), q);
        }


    } else if ( figure_name == "ROOK" )
    {

        std::string first_move;
        std::string last_first_move;
        getline(figure_stream, first_move, ' ');
        getline(figure_stream, last_first_move, ' ');

        Rook * r = new Rook(
                c, std::stoul(figure_y),
                std::stoul(figure_x),
                std::stoul(figure_last_x),
                std::stoul(figure_last_y),
                (figure_alive == "1")?1:0,
                (first_move == "1")?1:0,
                (last_first_move == "1")?1:0
        );


        if( c == WHITE)
        {
            this->white.figures.emplace(saved_name,r);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), r);
        } else{
            this->black.figures.emplace(saved_name,r);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), r);
        }


    } else if ( figure_name == "KING" )
    {
        std::string first_move;
        std::string prev_first_move;
        getline(figure_stream, first_move, ' ');
        getline(figure_stream, prev_first_move, ' ');

        King * k = new King(   c, std::stoul(figure_y),
                               std::stoul(figure_x),
                               std::stoul(figure_last_x),
                               std::stoul(figure_last_y),
                               (figure_alive == "1")?1:0,
                               (first_move == "1")?1:0,
                               (prev_first_move == "1")?1:0 );


        if( c == WHITE)
        {
            this->white.figures.emplace(saved_name,k);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), k);
        } else {
            this->black.figures.emplace(saved_name,k);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), k);
        }

    } else if ( figure_name == "BISHOP" )
    {
        Bishop * b= new  Bishop( c, std::stoul(figure_y),
                                 std::stoul(figure_x),
                                 std::stoul(figure_last_x),
                                 std::stoul(figure_last_y),
                                 (figure_alive == "1")?1:0 );

        if( c == WHITE)
        {
            this->white.figures.emplace(saved_name,b);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), b);
        } else {
            this->black.figures.emplace(saved_name,b);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), b);
        }

    } else if ( figure_name == "KNIGHT" )
    {
        Knight * k = new  Knight( c, std::stoul(figure_y),
                                  std::stoul(figure_x),
                                  std::stoul(figure_last_x),
                                  std::stoul(figure_last_y),
                                  (figure_alive == "1")?1:0 );


        if (c == WHITE)
        {
            this->white.figures.emplace(saved_name,k);
            this->white.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), k);
        } else {

            this->black.figures.emplace(saved_name,k);
            this->black.player_layout.emplace(std::make_pair(std::stoul(figure_x), std::stoul(figure_y)), k);
        }

    } else {

        return false;
    }

    return true;

}
//======================================================================================================================
unsigned Game::parseXPosition(const std::string &x) {

    if( x == "a" || x == "A" )
    {
        return 1;

    } else if( x == "b" || x == "B" )
    {
        return 2;

    } else if( x == "c" || x == "C" )
    {
        return 3;

    } else if( x == "d" || x == "D" )
    {
        return 4;

    } else if( x == "e" || x == "E" )
    {
        return 5;

    } else if( x== "f" || x == "F" )
    {
        return 6;

    } else if( x == "g" || x == "G" )
    {
        return 7;

    } else if (x == "h" || x == "H" ){

        return 8;
    }

    return -1;
}
//======================================================================================================================
bool Game::checkRoute( const unsigned & start_x , const unsigned  & start_y , const unsigned & end_x, const unsigned & end_y )
{
    unsigned x = start_x;
    unsigned y = start_y;


    while( x != end_x || y != end_y )
    {
        if( x != end_x )
        {
            if( start_x > end_x )
            {
                x--;

            } else {

                x++;
            }
        }

        if( y != end_y )
        {
            if( start_y > end_y )
            {
                y--;

            } else {

                y++;
            }
        }

        if( x == end_x && y == end_y )
            break;


        if( this->white.player_layout.find(std::make_pair(x,y)) != this->white.player_layout.end() )
        {
            return 0;
        }

        if(  this->black.player_layout.find(std::make_pair(x,y)) != this->black.player_layout.end() )
        {
            return 0;
        }

    }

    return 1;

}
//======================================================================================================================
bool Game::checkDestination(const unsigned &end_x, const unsigned &end_y , const COLOR & c , bool kill ) {

    if( c == BLACK )
    {
        if( this->black.player_layout.find(std::make_pair(end_x, end_y)) != this->black.player_layout.end() )
        {
            return 0;
        }

        auto white_figure = this->white.player_layout.find(std::make_pair(end_x, end_y));

        if( white_figure != this->white.player_layout.end() )
        {
            if( kill )
            {
                (white_figure->second)->killFigure();
                this->white.player_layout.erase(white_figure);
            }


        }


    } else {

        if( this->white.player_layout.find(std::make_pair(end_x, end_y)) != this->white.player_layout.end() )
        {
            return 0;
        }

        auto black_figure = this->black.player_layout.find(std::make_pair(end_x, end_y));

        if( black_figure != this->black.player_layout.end() )
        {
            if( kill )
            {
                (black_figure->second)->killFigure();
                this->black.player_layout.erase(black_figure);
            }

        }

    }

    return 1;

}
//======================================================================================================================
bool Game::validCastling( const unsigned &x, const unsigned &y, bool makeMove  ) {

    //white
    if( x == 7  && y == 1 )
    {
        //check positions
        //[ 6, 1 ]
        if( !( this->white.player_layout.find(std::make_pair(6,1)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(6,1)) == this->white.player_layout.end() && this->isPositionSafe(6,1, WHITE)  ) )
        {
            return false;
        }

        //[ 7, 1 ]
        if( !( this->white.player_layout.find(std::make_pair(7,1)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(7,1)) == this->white.player_layout.end() && this->isPositionSafe(7,1, WHITE)  ) )
        {
            return false;
        }

        auto rook = this->white.player_layout.find(std::make_pair(8,1));
        auto king = this->white.player_layout.find(std::make_pair(5,1));

        std::pair<unsigned,unsigned> rook_coordinates = (rook->second)->getCoordinates();
        std::pair<unsigned,unsigned> king_coordinates = (king->second)->getCoordinates();

        if ( rook == this->white.player_layout.end() || king == this->white.player_layout.end() )
        {
            return false;
        }

       if ( makeMove )
       {
           if( !(rook->second->setPosition(6,1) && king->second->setPosition(7,1)) )
           {
               return false;
           }

           this->white.player_layout.emplace(std::make_pair(7,1),king->second );
           this->white.player_layout.erase(std::make_pair(king_coordinates.first, king_coordinates.second));

           this->white.player_layout.emplace(std::make_pair(6,1),rook->second );
           this->white.player_layout.erase(std::make_pair(rook_coordinates.first, rook_coordinates.second));
       }

        return true;
    }

    if( x == 3  && y == 1 )
    {
        //check positions

        //[ 4, 1 ]

        if( !( this->white.player_layout.find(std::make_pair(4,1)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(4,1)) == this->black.player_layout.end() && this->isPositionSafe(4,1, WHITE)  ) )
        {
            return false;
        }

        //[ 3, 1 ]
        std::cout << (this->white.player_layout.find(std::make_pair(3,1)) == this->white.player_layout.end()) << std::endl;
        std::cout << (this->black.player_layout.find(std::make_pair(3,1)) == this->white.player_layout.end()) << std::endl;
        std::cout << this->isPositionSafe(3,1, WHITE) << std::endl;

        if( !( this->white.player_layout.find(std::make_pair(3,1)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(3,1)) == this->black.player_layout.end() && this->isPositionSafe(3,1, WHITE)  ) )
        {
            return false;
        }

        auto rook = this->white.player_layout.find(std::make_pair(1,1));
        auto king = this->white.player_layout.find(std::make_pair(5,1));

        std::pair<unsigned,unsigned> rook_coordinates = (rook->second)->getCoordinates();
        std::pair<unsigned,unsigned> king_coordinates = (king->second)->getCoordinates();

        if ( rook == this->white.player_layout.end() || king == this->white.player_layout.end() )
        {
            return false;
        }

        if ( makeMove )
        {
            if( !(rook->second->setPosition(4,1) && king->second->setPosition(3,1)) )
            {
                return false;
            }

            this->white.player_layout.emplace(std::make_pair(3,1),king->second );
            this->white.player_layout.erase(std::make_pair(king_coordinates.first, king_coordinates.second));

            this->white.player_layout.emplace(std::make_pair(4,1),rook->second );
            this->white.player_layout.erase(std::make_pair(rook_coordinates.first, rook_coordinates.second));
        }

        return true;

    }

    //black
    if( x == 7  && y == 8 )
    {
        //check positions

        //[ 6, 8 ]
        if( !( this->white.player_layout.find(std::make_pair(6,8)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(6,1)) == this->black.player_layout.end() && this->isPositionSafe(6,8, BLACK)  ) )
        {
            return false;
        }

        //[ 7, 8 ]
        if( !( this->white.player_layout.find(std::make_pair(7,8)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(7,1)) == this->black.player_layout.end() && this->isPositionSafe(7,8, BLACK)  ) )
        {
            return false;
        }

        auto rook = this->black.player_layout.find(std::make_pair(8,8));
        auto king = this->black.player_layout.find(std::make_pair(5,8));

        std::pair<unsigned,unsigned> rook_coordinates = (rook->second)->getCoordinates();
        std::pair<unsigned,unsigned> king_coordinates = (king->second)->getCoordinates();

        if ( rook == this->black.player_layout.end() || king == this->black.player_layout.end() )
        {
            return false;
        }

        if ( makeMove )
        {
            if( !(rook->second->setPosition(6,8) && king->second->setPosition(7,8)) )
            {
                return false;
            }

            this->black.player_layout.emplace(std::make_pair(7,8),king->second );
            this->black.player_layout.erase(std::make_pair(king_coordinates.first, king_coordinates.second));

            this->black.player_layout.emplace(std::make_pair(6,8),rook->second );
            this->black.player_layout.erase(std::make_pair(rook_coordinates.first, rook_coordinates.second));
        }

        return true;


    }

    if( x == 3  && y == 8 )
    {
        //check positions

        //[ 4, 8 ]
        if( !( this->white.player_layout.find(std::make_pair(4,8)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(4,8)) == this->black.player_layout.end() && this->isPositionSafe(3,8, BLACK)  ) )
        {
            return false;
        }

        //[ 3, 8 ]
        if( !( this->white.player_layout.find(std::make_pair(3,8)) == this->white.player_layout.end() && this->black.player_layout.find(std::make_pair(3,8)) == this->black.player_layout.end() && this->isPositionSafe(3,8, BLACK)  ) )
        {
            return false;
        }

        auto rook = this->black.player_layout.find(std::make_pair(1,8));
        auto king = this->black.player_layout.find(std::make_pair(5,8));

        std::pair<unsigned,unsigned> rook_coordinates = (rook->second)->getCoordinates();
        std::pair<unsigned,unsigned> king_coordinates = (king->second)->getCoordinates();

        if ( rook == this->black.player_layout.end() || king == this->black.player_layout.end() )
        {
            return false;
        }

        if ( makeMove )
        {
            if( !(rook->second->setPosition(4,8) && king->second->setPosition(3,8)) )
            {
                 return false;
            }

            this->black.player_layout.emplace(std::make_pair(3,8),king->second );
            this->black.player_layout.erase(std::make_pair(king_coordinates.first, king_coordinates.second));

            this->black.player_layout.emplace(std::make_pair(4,8),rook->second );
            this->black.player_layout.erase(std::make_pair(rook_coordinates.first, rook_coordinates.second));
        }

        return true;
    }

    return 0;

}
//======================================================================================================================
bool Game::isPositionSafe(const unsigned &x, const unsigned &y, const COLOR & c ) {

    if( c == WHITE )
    {
        for( auto figure : this->black.player_layout )
        {
            if( (figure.second)->threatenPosition(x,y) )
            {
                return false;
            }
        }


    } else {

        for( auto figure : this->white.player_layout )
        {
            if( (figure.second)->threatenPosition(x,y) )
            {
                return false;
            }
        }
    }

    return true;

}
//======================================================================================================================
void Game::promotePawn( Figure & p, const COLOR & c ) {

    std::string pawn_name;
    std::pair<unsigned, unsigned> pawn_position = p.getCoordinates();

    if ( c == WHITE )
    {
        for(auto f : this->white.figures )
        {
            if ( *(f.second) == (p) )
            {
                pawn_name = f.first;

                delete f.second;

                break;
            }
        }

    } else {

        for(auto f : this->black.figures )
        {
            if ( *(f.second) == (p) )
            {
                pawn_name = f.first;

                delete f.second;

                break;
            }
        }
    }

    char tmp = pawn_name[1];

    int pawn_number = tmp - '0';

    if ( c == WHITE )
    {
        this->white.figures.erase(pawn_name);
        this->white.player_layout.erase(pawn_position);

        Queen * q = new Queen(pawn_position.first, pawn_position.second, WHITE);
        this->white.player_layout.emplace(pawn_position, q);

        std::string queen_name = "q" + std::to_string(pawn_number) ;

        this->white.figures.emplace(queen_name, q);

    } else {

        this->black.figures.erase(pawn_name);
        this->black.player_layout.erase(pawn_position);

        Queen * q = new Queen(pawn_position.first, pawn_position.second, WHITE);
        this->black.player_layout.emplace(pawn_position, q);

        std::string queen_name = "q" + std::to_string(pawn_number) ;

        this->black.figures.emplace(queen_name, q);
    }

}
//======================================================================================================================
void Game::Print() {

    std::string chess_board = "";
    chess_board+="     +------+------+------+------+------+------+------+------+\n ";
    chess_board+="     |   A  |   B  |   C  |   D  |   E  |   F  |   G  |   H  |\n ";
    chess_board+="     +------|------|------|------|------|------|------|------+\n ";
    chess_board+="                                                              \n ";
    chess_board+="+-+  +------|------|------|------|------|------|------|------+\n ";

    for(int y = 7; y >= 0; y-- )
    {
        std::string row_1;
        std::string row_2;
        std::string row_3;

        row_1+=("|"+std::to_string(y+1)+"|  ");
        row_2+=" | |  ";

        for(unsigned x = 0; x < 8; x++)
        {
            auto white_figure = this->white.player_layout.find(std::make_pair(x+1, y+1));
            auto black_figure = this->black.player_layout.find(std::make_pair(x+1, y+1));

            if ( white_figure != this->white.player_layout.end() )
            {
                std::string figure_name = (white_figure->second)->getName();

                if( x == 0 )
                {
                    row_1+="|";
                    row_2+="|";
                }

                row_1+=figure_name;
                row_2+="W     ";

                for ( int padding = figure_name.size(); padding < 6; padding++)
                {
                    row_1+=" ";
                }

                row_1+="|";
                row_2+="|";

                continue;

            }

            if( black_figure != this->black.player_layout.end() )
            {
                std::string figure_name = (black_figure->second)->getName();

                if( x == 0 )
                {
                    row_1+="|";
                    row_2+="|";
                }

                row_1+=figure_name;
                row_2+="B     ";

                for ( int padding = figure_name.size(); padding < 6; padding++)
                {
                    row_1+=" ";
                }

                row_1+="|";
                row_2+="|";

                continue;
            }

            if( x == 0 )
            {
                row_1+="|      |";
                row_2+="|      |";

            } else {
                row_1+="      |";
                row_2+="      |";
            }
        }

        chess_board+=row_1+"\n";
        chess_board+=row_2+"\n";
        chess_board+=" +-|  |------|------|------|------|------|------|------|------|\n ";
    }

    std::cout << chess_board << std::endl;


    std::cout << ":::WHITE:::\n";

    for(auto const & i : this->white.figures)
    {
        if( i.second->isAlive() )
        {
            std::cout << i.first << " -- " <<  *(i.second);
        }
    }

    std::cout << "\n";

    std::cout << ":::BLACK:::\n";

    for(auto const & i : this->black.figures)
    {
        if( i.second->isAlive() )
        {
            std::cout << i.first << " -- " <<  *(i.second);
        }
    }
}
//======================================================================================================================