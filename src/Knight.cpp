#include "Knight.hpp"

//======================================================================================================================
Knight::Knight(const unsigned &x, const unsigned &y, const COLOR & color ) {

    this->x = x;
    this->y = y;
    this->name = "KNIGHT";
    this->alive = true;
    this->c = color;
}
//======================================================================================================================
Knight::Knight() {

    this->name = "KNIGHT";
    this->alive = true;
}
//======================================================================================================================
Knight::Knight(const Knight &k) {

    this->x  = k.x;
    this->y  = k.y;
    this->c = k.c;
    this->name = "KNIGHT";
    this->alive = true;
    this->last_position = k.last_position;
}
//======================================================================================================================
Knight::Knight(const COLOR &c, const unsigned &x, const unsigned &y, const unsigned &last_x, const unsigned &last_y,
               const bool &alive) {

    this->x  = x;
    this->y  = y;
    this->c = c;
    this->name = "KNIGHT";
    this->alive = alive;
    this->last_position = std::make_pair(last_x,last_y);
}
//======================================================================================================================
Knight& Knight::operator=(const Knight &k) {

    this->x  = k.x;
    this->y  = k.y;
    this->c = k.c;
    this->name = "KNIGHT";
    this->alive = true;
    this->last_position = k.last_position;

    return *this;
}
//======================================================================================================================
bool Knight::setPosition(const unsigned &x, const unsigned &y) {

    if( x > 8 || x < 1 || y > 8 || y < 1 )
        return 0;

    if( ( abs((int)(this->y) - (int)(y)) == 2 && abs((int)(this->x) - (int)(x)) == 1 ) || ( abs((int)(this->x) - (int)(x)) == 2 && abs((int)(this->y) - (int)(y)) == 1 ) )
    {
        this->last_position = std::make_pair(this->x, this->y);

        this->x = x;
        this->y = y;

        return 1;
    }

    return 0;
}
//======================================================================================================================
bool Knight::threatenPosition(unsigned x, unsigned y) {

    if( ( abs((int)(this->y) - (int)(y)) == 2 && abs((int)(this->x) - (int)(x)) == 1 ) || ( abs((int)(this->x) - (int)(x)) == 2 && abs((int)(this->y) - (int)(y)) == 1 ) )
        return 1;

    return 0;

}
//======================================================================================================================
std::set<std::pair<std::string , unsigned >> Knight::calculateAllPossibleMoves  ()
{
    std::set<std::pair<std::string, unsigned >> all_moves;

    if( (this->y + 2) >= 1 && (this->y + 2) <= 8 )
    {
        if( (this->x + 1) >= 1 && (this->x + 1) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(this->x + 1), this->y + 2));

        if( (this->x - 1) >= 1 && (this->x - 1) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1), this->y + 2));
    }

    if( (this->y - 2) >= 1 && (this->y - 2) <= 8 )
    {
        if( (this->x + 1) >= 1 && (this->x + 1) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(this->x + 1), this->y - 2));

        if( (this->x - 1) >= 1 && (this->x - 1) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1), this->y - 2));
    }

    if ( ( this->x + 2) >= 1 && (this->x + 2) <= 8 )
    {
        if( (this->y + 1) >= 1 && (this->y + 1) <= 8 )
            all_moves.emplace(std::make_pair( this->unsignedXToString(this->x + 2), this->y + 1) );

        if( (this->y - 1) >= 1 && (this->y - 1) <= 8 )
            all_moves.emplace(std::make_pair( this->unsignedXToString(this->x + 2), this->y - 1) );
    }

    if ( ( this->x - 2) >= 1 && (this->x - 2) <= 8 )
    {
        if( (this->y + 1) >= 1 && (this->y + 1) <= 8 )
            all_moves.emplace(std::make_pair( this->unsignedXToString(this->x - 2), this->y + 1) );

        if( (this->y - 1) >= 1 && (this->y - 1) <= 8 )
            all_moves.emplace(std::make_pair( this->unsignedXToString(this->x - 2), this->y - 1) );
    }

    return all_moves;
}
//======================================================================================================================
Knight* Knight::Copy() {
    return new Knight(*this);
}
//======================================================================================================================