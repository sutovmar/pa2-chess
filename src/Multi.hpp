#pragma once

#include "Game.hpp"

class Multi : public Game
{
    public:
    //==================================================================================================================
        /**
         * Inicial constructor for multiplayer
         */
        Multi           ();
    //==================================================================================================================
        /**
         * Actual playing
         */
        void Play       ();
    //==================================================================================================================
};