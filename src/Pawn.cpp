#include "Pawn.hpp"

//======================================================================================================================
Pawn::Pawn() {
    this->name = "PAWN";
    this->alive = true;
    this->double_move = false;
    this->first_move = false;
}
//======================================================================================================================
Pawn::Pawn(const unsigned &x, const unsigned &y, const COLOR & color ) {

    this->x = x;
    this->y = y;
    this->name = "PAWN";
    this->alive = true;
    this->c = color;
    this->double_move = false;
    this->first_move = false;
}
//======================================================================================================================
Pawn::Pawn(const COLOR &c, const unsigned &x, const unsigned &y, const unsigned &last_x, const unsigned &last_y,const bool & alive,
           const bool &double_move, const bool &first_move, const bool &prev_double_move,
           const bool &prev_first_move) {

    this->x  = x;
    this->y  = y;
    this->name = "PAWN";
    this->alive = alive;
    this->c = c;
    this->double_move = double_move;
    this->first_move  = first_move;
    this->prev_double_move =prev_double_move;
    this->prev_double_move =prev_double_move;
    this->last_position = std::make_pair(last_x, last_y);
}
//======================================================================================================================
Pawn::Pawn(const Pawn &p) {
    this->x  = p.x;
    this->y  = p.y;
    this->name = "PAWN";
    this->alive = true;
    this->c = p.c;
    this->double_move = p.double_move;
    this->first_move  = p.first_move;
    this->last_position = std::make_pair(p.last_position.first,p.last_position.second);
}
//======================================================================================================================
Pawn& Pawn::operator=(const Pawn &p) {
    this->x  = p.x;
    this->y  = p.y;
    this->name = "PAWN";
    this->alive = true;
    this->c = p.c;
    this->double_move = p.double_move;
    this->first_move  = p.first_move;
    this->last_position = std::make_pair(p.last_position.first,p.last_position.second);

    return *this;
}
//======================================================================================================================
bool Pawn::setPosition(const unsigned &x, const unsigned &y) {

    if( x > 8 || x < 1 || y > 8 || y < 1 )
        return 0;

    if( !this->double_move && !this->first_move )
    {
        if( this->c == BLACK && (y == this->y - 1 || y == this->y - 2 ) )
        {
            if ( y == this->y - 2)
                this->last_position = std::make_pair(this->x, this->y - 1);
            else
                this->last_position = std::make_pair(this->x, this->y );

            this->prev_double_move = this->double_move;
            this->prev_first_move = this->first_move;
            this->x = x;
            this->y = y;
            this->double_move = true;
            this->first_move = true;

            return 1;
        }

        if( this->c == WHITE && (y == this->y + 1 || y == this->y + 2 ) )
        {
            if ( y == this->y + 2)
                this->last_position = std::make_pair(this->x, this->y + 1);
            else
                this->last_position = std::make_pair(this->x, this->y );

            this->prev_double_move = this->double_move;
            this->prev_first_move = this->first_move;
            this->x = x;
            this->y = y;
            this->double_move = true;
            this->first_move = true;

            return 1;
        }

        return 0;
    }

    if( this->c == BLACK && y == this->y - 1 && abs((int)(x) - (int)(this->x)) <= 1 )
    {
        this->last_position = std::make_pair(this->x, this->y);
        this->prev_double_move = this->double_move;
        this->prev_first_move = this->first_move;
        this->double_move = false;
        this->x = x;
        this->y = y;

        return 1;
    }

    if( this->c == WHITE && y == this->y + 1 && abs((int)(x) - (int)(this->x)) <= 1  )
    {
        this->last_position = std::make_pair(this->x, this->y);
        this->prev_double_move = this->double_move;
        this->prev_first_move = this->first_move;
        this->double_move = false;
        this->x = x;
        this->y = y;

        return 1;
    }

    return 0;
}
//======================================================================================================================
bool Pawn::threatenPosition(unsigned x, unsigned y)  {

    if ( this->c == BLACK )
    {
        if( (x == this->x + 1 && y == this->y - 1) || (x == this->x - 1 && y == this->y - 1))
            return true;

        return false;

    } else {

        if( (x == this->x + 1 && y == this->y + 1) || (x == this->x - 1 && y == this->y + 1))
            return true;

        return false;
    }
}
//======================================================================================================================
std::pair<unsigned int, unsigned int> Pawn::getLastPosition() const {

    return this->last_position;
}
//======================================================================================================================
std::set<std::pair<std::string , unsigned >> Pawn::calculateAllPossibleMoves  ()
{
    std::set<std::pair<std::string, unsigned >> all_moves;

    if ( (this->y + 1) >= 1 &&  (this->y + 1) <= 8 )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x), this->y+1));

    if( !(this->double_move)  )
    {
        if ( (this->y + 2) >= 1 &&  (this->y + 2) <= 8 )
            all_moves.emplace(std::make_pair(this->unsignedXToString(this->x), this->y+2));
    }

    if ( (this->y + 1) >= 1 &&  (this->y + 1) <= 8 && (this->x + 1) >= 1 &&  (this->x + 1) <= 8)
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x+1), this->y+1));

    if ( (this->y + 1) >= 1 &&  (this->y + 1) <= 8 && (this->x - 1) >= 1 &&  (this->x - 1) <= 8)
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1), this->y + 1));

    return all_moves;
}
//======================================================================================================================
std::string Pawn::saveFigure() {

    std::string figure_data;

    figure_data+=std::to_string(this->y) + " ";
    figure_data+=std::to_string(this->x) + " ";
    figure_data+=std::to_string(this->alive) + " ";
    figure_data+=std::to_string(this->last_position.first) + " ";
    figure_data+=std::to_string(this->last_position.second) + " ";
    figure_data+=std::to_string(this->first_move) + " ";
    figure_data+=std::to_string(this->double_move) + " ";
    figure_data+=std::to_string(this->prev_first_move) + " ";
    figure_data+=std::to_string(this->prev_double_move);

    return figure_data;
}
//======================================================================================================================
bool Pawn::wasLastMoveDouble() const {
    return this->double_move;
}
//======================================================================================================================
void Pawn::undoMove() {
    this->x             = this->last_position.first;
    this->y             = this->last_position.second;
    this->first_move    = this->prev_first_move;
    this->double_move   = this->prev_double_move;
}
//======================================================================================================================
Pawn* Pawn::Copy() {
    return new Pawn(*this);
}