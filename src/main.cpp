#include <iostream>
#include "Multi.hpp"
#include "Single.hpp"

using namespace std;

int main()
{
    std::cout << "##################################################################" << std::endl;
    std::cout << " ________    __    __    _______    _______    ________   " << std::endl;
    std::cout << "|   _____|  |  |  |  |  |  _____|  |  _____|  |   _____|  " << std::endl;
    std::cout << "|  |        |  |__|  |  |  |____   | |_____   |  |_____  " << std::endl;
    std::cout << "|  |        |   __   |  |   ____|  |______  | |______  | " << std::endl;
    std::cout << "|  |_____   |  |  |  |  |  |____    ______| |  ______| | " << std::endl;
    std::cout << "|________|  |__|  |__|  |_______|  |________| |________| " << std::endl;
    std::cout << "                                                                  " << std::endl;
    std::cout << "##################################################################" << std::endl;

    std::cout << "\n";
    std::cout << "\n";
    std::cout << "\n";

    string game_mode;

    cout << "Choose game mode : " << " SINGLE - [ playing againts AI ] | " << " MULTI - [ playing against other player ] " << endl;

    cin.clear();
    cin >> game_mode;

    locale loc;
    string lower_command;

    for( auto c : game_mode )
    {
        lower_command=lower_command+std::tolower(c,loc);
    }

    while ( cin.fail() || (lower_command != "single" && lower_command != "multi") )
    {
        cin.clear();

        cin.ignore(1024, '\n');

        lower_command = "";

        cout << "Wrong input please, try again" << endl;

        cin >> game_mode;

        for( auto c : game_mode )
        {
            lower_command=lower_command+std::tolower(c,loc);
        }
    }

    if ( lower_command == "single" )
    {
        Single  s;
        s.Play();

    } else {

        Multi   m;
        m.Play();
    }


    return 0;

}