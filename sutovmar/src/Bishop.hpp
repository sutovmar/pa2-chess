#pragma once

#include "Figure.hpp"

class Bishop : public Figure
{

public:
    //==================================================================================================================
    /**
    * Constructor for initialization of chessboard
    * @param x - starting X position
    * @param y - starting Y position
    * @param color - color of player
    */
    Bishop                                                                      ( const unsigned & x,
                                                                                  const unsigned & y,
                                                                                  const COLOR & color );
    //==================================================================================================================
    /**
    * Empty constructor
    */
    Bishop                                                                      ();
    //==================================================================================================================
    /**
    * Copy constructor
    * @param b - object from copy
    */
    Bishop                                                                      ( const Bishop & b );
    //==================================================================================================================
    /**
     * Constructor for recreating board from loaded file
     * @param c - color of player
     * @param x - saved X position of figure
     * @param y - saved Y position of figure
     * @param last_x - saved previous X position of figure
     * @param last_y - saved previous Y position of figure
     * @param alive - saved value if figure was alive
     */
    Bishop                                                                      (   const COLOR & c,
                                                                                    const unsigned & x,
                                                                                    const unsigned & y,
                                                                                    const unsigned & last_x,
                                                                                    const unsigned & last_y ,
                                                                                    const bool & alive);
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param b - object to copy from
    * @return - copied object
    */
    Bishop & operator=                                                          ( const Bishop & b );
    //==================================================================================================================
    /**
    * Set position for figure
    * @param x - new X position
    * @param y - new Y position
    * @return true if position was valid and set, false if not
    */
    bool setPosition                                                            ( const unsigned & x,
                                                                                  const unsigned & y )         override;
    //==================================================================================================================
    /**
    * Check if figure threaten certain position
    * @param x - X position to threaten
    * @param y - Y position to threaten
    * @return - true if threaten, false if not
    */
    bool threatenPosition                                                       ( unsigned x , unsigned y )    override;

    //==================================================================================================================
    /**
     * Generate all possible moves ( even not valid on current chessboard )
     * @return map of possible moves
     */
    std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves     ()                            override;
    //==================================================================================================================
    /**
     * Copy function
     * @return copied object
     */
     Bishop* Copy() override;
    //==================================================================================================================
};