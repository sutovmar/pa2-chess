#include "Figure.hpp"

//======================================================================================================================
Figure::Figure(const Figure &f) {

    this->x  = f.x;
    this->y  = f.y;
    this->alive = true;
    this->c = f.c;
}
//======================================================================================================================
Figure& Figure::operator=(const Figure &f) {

    this->x = f.x;
    this->y = f.y;
    this->c = f.c;

    return *this;
}
//======================================================================================================================
void Figure::killFigure() {
    this->alive = false;
}
//======================================================================================================================
bool Figure::setPosition(const unsigned &x, const unsigned &y) {

    if( x > 8 || x < 1 || y > 8 || y < 1 )
        return 0;

    this->x = x;
    this->y = y;

    return 1;
}
//======================================================================================================================
std::string Figure::unsignedXToString(const unsigned & x) const {

    std::string out = "";

    switch ( x )
    {
        case 1:
            out = "A";
            break;

        case 2:
            out = "B";
            break;

        case 3:
            out = "C";
            break;

        case 4:
            out = "D";
            break;

        case 5:
            out = "E";
            break;

        case 6:
            out = "F";
            break;

        case 7:
            out = "G";
            break;

        case 8:
            out = "H";
            break;

        default:
            std::cout << "Throwing exceptions" << std::endl;
            break;
    }

    return out;
}
//===================================================================== =================================================
std::pair<unsigned int, unsigned int> Figure::getCoordinates() const {
    return std::make_pair(this->x, this->y);
}
//======================================================================================================================
bool Figure::isAlive() const {
    return this->alive;
}
//======================================================================================================================
bool Figure::isPawn() const {
    return (this->name == "PAWN");
}
//======================================================================================================================
bool Figure::isKing() const {
    return (this->name == "KING");
}
//======================================================================================================================
bool Figure::isRook() const {
    return (this->name == "ROOK");
}
//======================================================================================================================
bool Figure::isBishop() const {
    return (this->name == "BISHOP");
}
//======================================================================================================================
bool Figure::isQueen() const {
    return (this->name == "QUEEN");
}
//======================================================================================================================
bool Figure::isKnight() const {
    return (this->name == "KNIGHT");
}
//======================================================================================================================
std::string Figure::saveFigure() {

    std::string figure_data;

    figure_data+=std::to_string(this->y) + " ";
    figure_data+=std::to_string(this->x) + " ";
    figure_data+=std::to_string(this->alive) + " ";
    figure_data+=std::to_string(this->last_position.first) + " ";
    figure_data+=std::to_string(this->last_position.second);

    return figure_data;
}
//======================================================================================================================
void Figure::undoMove() {
    this->x = this->last_position.first;
    this->y = this->last_position.second;
}
//======================================================================================================================
std::string Figure::getName() {
    return this->name;
}
//======================================================================================================================
Figure* Figure::Copy() {
    return new Figure(*this);
}
//======================================================================================================================

