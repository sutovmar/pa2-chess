#pragma once

#include <string>
#include <iostream>
#include <set>

enum COLOR
{
    BLACK,
    WHITE
};

class Figure {

public:

    //==================================================================================================================
    /**
     * Constructor for initialization of chessboard
     * @param x - starting X position
     * @param y - starting Y position
     */
    Figure( const unsigned & x, const unsigned & y): x(x), y(y) {}
    //==================================================================================================================
    /**
     * Copy constructor
     * @param f - object to copy from
     */
    Figure( const Figure & f );
    //==================================================================================================================
    /**
     * Empty constructor
     */
    Figure () = default;
    //==================================================================================================================
    /**
     * Destructor
     */
    virtual ~Figure() = default;
    //==================================================================================================================
    /**
     * Copy function
     * @return copied object
     */
    virtual Figure* Copy();
    //======================================================================================================================
    /**
     * Overloaded operator =
     * @param f - object to copy from
     * @return - copied object
     */
    Figure& operator=( const Figure & f );
    //==================================================================================================================
    /**
     * Kills figure
     */
    void killFigure ();
    //==================================================================================================================
    /**
     * Function for setting position of figure
     * @param x - next X position
     * @param y - next Y position
     * @return - true if position is valid, false if not
     */
    virtual bool setPosition(const unsigned & x, const unsigned & y );
    //==================================================================================================================
    /**
     * Get pair of X and Y position
     * @return pair<X_position, Y_position>
     */
    std::pair<unsigned , unsigned > getCoordinates() const;
    //==================================================================================================================
    /**
     * Return string with attributes of figure for saving in file
     * @return
     */
    virtual std::string saveFigure                        ();
    //==================================================================================================================
    /**
     * Checks if figure is alive
     * @return
     */
    bool isAlive                () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of Knight
     * @return
     */
    bool isKnight               () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of Pawn
     * @return
     */
    bool isPawn                 () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of King
     * @return
     */
    bool isKing                 () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of Bishop
     * @return
     */
    bool isBishop               () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of Queen
     * @return
     */
    bool isQueen                () const;
    //==================================================================================================================
    /**
     * Checks if figure is type of Rook
     * @return
     */
    bool isRook                 () const;
    //==================================================================================================================
    /**
     * Checks if figure threaten another figure on X position and Y position
     * @param x - X position to threaten
     * @param y - Y position to threaten
     * @return true if threaten, false if not
     */
    virtual bool threatenPosition( unsigned x, unsigned y) {
        return true;
    };
    //==================================================================================================================
    /**
     * Generate all possible moves ( even not valid on current chessboard )
     * @return map of possible moves
     */
    virtual std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves   () {

        std::set<std::pair<std::string, unsigned >> all_moves;

        return all_moves;

    };
    //==================================================================================================================
    /**
     * Return figure to state before
     *
     */
    virtual void undoMove                 ();
    //==================================================================================================================
    /**
     * overloaded operator <<
     * @param x stream
     * @param f figure
     * @return stream with added info about figure
     */
    friend std::ostream& operator<< ( std::ostream & x, const Figure & f )
    {
        x << f.name << " : " << f.unsignedXToString(f.x) << " " <<  f.y << "\n";

        return x;
    };
    //==================================================================================================================
    /**
     * Overloaded operator == for comparing
     * @param f1 - first figure
     * @param f2 - second figure
     * @return - true if are same, false if not
     */
    friend bool operator==(const Figure & f1, const Figure & f2 )
    {
        return ( f1.x == f2.x && f1.y == f2.y && f1.c == f2.c);
    }
    //======================================================================================================================
    /**
     * Get name ( type ) of figure
     * @return
     */
    std::string getName             ();
    //======================================================================================================================

protected:

    //==================================================================================================================
    unsigned x, y;
    std::string name;
    bool alive;
    COLOR c;
    std::pair<unsigned, unsigned > last_position;
    //==================================================================================================================
    /**
     * Parse unsigned X position to string ( A,B,..)
     * @param x - X position to parse
     * @return - string representing X position
     */
    std::string unsignedXToString ( const unsigned & x ) const ;
    //==================================================================================================================
};