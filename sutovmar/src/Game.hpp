#pragma once

#include "Pawn.hpp"
#include "Rook.hpp"
#include "Bishop.hpp"
#include "Knight.hpp"
#include "Queen.hpp"
#include "King.hpp"

#include <iostream>
#include <map>
#include <string>
#include <set>
#include <random>
#include <locale>
#include <fstream>
#include <sstream>

//======================================================================================================================
/**
 * Structure for saving info about white or black player
 */
struct Player_Figures
{
    std::map<std::string, Figure*>                                           figures;
    std::map<std::pair<unsigned ,unsigned>, Figure*>                         player_layout;

    Player_Figures() {

    };
};
//======================================================================================================================

class Game
{
public:

    //==================================================================================================================
    /**
     * Empty constructor
     */
    Game() = default;
    //==================================================================================================================
    /**
     * Destructor
     */
    ~Game();
    //==================================================================================================================
    /**
     * Check if black or white king is in checkmate
     * @param c - color for deciding which king check
     * @return - true if is check mate , false if not for certain color
     */
    bool isCheckMate                    ( const COLOR & c );
    //==================================================================================================================
    /**
     * Check if black or white king is in check position
     * @param c - color for deciding which king to check
     * @return - true if is in check, false if not
     */
    bool isCheck                        ( const COLOR & c );
    //==================================================================================================================
    /**
     * Check if valid is move on current chessboard
     * @param color - which player is performing move
     * @param figure - which figure is performing move
     * @param x - X position where to move figure
     * @param y - Y position where to move figure
     * @param action - actually perform move ( or is just checking if move is valid on current checkboard)
     * @param log - setting on or off printing logs about move
     * @return true if move is valid ( or performed ), false if not
     */
    bool validMove                      (   COLOR color, std::string figure,
                                            std::string x,
                                            unsigned y,
                                            bool action = false,
                                            bool log = true );
    //==================================================================================================================
    /**
     * Parse string command for player
     * @param command - what command perform
     * @param color - which player is performin command
     * @return - 0 for exiting from game, 2 for saving current chessboard, 1 for move ( which is valid ) and 0 for move
     * which is not valid
     */
    int parseCommand                   ( std::string &command, const COLOR & color );
    //==================================================================================================================
    /**
     * Print current layout of chessboard
     */
    void Print                          ();
    //==================================================================================================================
    /**
     * Save current chessboard to file
     * @return - true if chessboard was saved, false if not
     */
    bool SaveGame                       ();
    //==================================================================================================================
    /**
     * Load chessboard from file
     * @return - true if loading was successful, false if not
     */
    bool LoadGame                       ( const char * filename );
    //==================================================================================================================
    /**
     * Prompts when game starts
     */
    void startGame                      ();
    //==================================================================================================================
    /**
     * Load figure from file and place it on current chessboard
     * @param figure  - type of figure
     * @param c  - under which color ( player ) saved her
     * @return  - true if loading figure was successful, false if not
     */
    bool LoadFigure                     (std::string figure, const COLOR & c);
    //==================================================================================================================

protected:

    //==================================================================================================================
    Player_Figures white;

    Player_Figures black;
    //==================================================================================================================
    /**
     * Load white figures on default positions
     */
    void InitWhite                      ();
    //==================================================================================================================
    /**
     * Load black figures on default positions
     */
    void InitBlack                      ();
    //==================================================================================================================
    /**
     * Convert string X position to number representative
     * @param x - which position convert
     * @return - string representing X position
     */
    unsigned parseXPosition             ( const std::string & x );
    //==================================================================================================================
    /**
     * Check if route of figure is safe and if something is not standing in the way
     * @param start_x - starting X point
     * @param start_y - starting Y point
     * @param end_x - ending X point
     * @param end_y - ending Y point
     * @return true if route is valid, false if not
     */
    bool checkRoute                     (   const unsigned & start_x ,
                                            const unsigned  & start_y ,
                                            const unsigned & end_x,
                                            const unsigned & end_y );
    //==================================================================================================================
    /**
     * Check if destination is safe and valid for certain color (player)
     * @param end_x - X position of destination
     * @param end_y - Y position of destination
     * @param c - if is white or black player
     * @return - true if it is valid and safe, false if not
     */
    bool checkDestination               ( const unsigned & end_x, const unsigned & end_y , const COLOR & c, bool kill = true );
    //==================================================================================================================
    /**
     * Checks if castling for figures is valid
     * @param x - X position for castling
     * @param y - Y position for castling
     * @param makeMove - to check if moved should be performed or if it is just checking validity of move
     * @return - true if castling is valid, false if not
     */
    bool validCastling                  ( const unsigned & x, const unsigned & y, bool makeMove = true );
    //==================================================================================================================
    /**
     * Checks if position is safe for certain color (player)
     * @param x - X position to check
     * @param y - Y position to check
     * @param c - color of player
     * @return - true if it is safe, false if not
     */
    bool isPositionSafe                 ( const unsigned & x, const unsigned & y, const COLOR & c );
    //==================================================================================================================
    /**
     * Promote pawn to queen
     * @param p - figure to promote
     * @param c - under which player promote
     */
    void promotePawn                    (  Figure & p, const COLOR & c);
    //==================================================================================================================
};