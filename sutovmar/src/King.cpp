#include "King.hpp"

//======================================================================================================================
King::King(const unsigned &x, const unsigned &y, const COLOR & color) {

    this->name = "KING";
    this->x  = x;
    this->y = y;
    this->alive = true;
    this->c = color;
    this->first_move = false;
}
//======================================================================================================================
King::King(const COLOR &c, const unsigned &x, const unsigned &y, const unsigned &last_x, const unsigned &last_y,
           const bool &alive, const bool &first_move, const bool &prev_first_move) {

    this->name = "KING";
    this->x  = x;
    this->y = y;
    this->alive = alive;
    this->c = c;
    this->first_move = first_move;
    this->prev_first_move = prev_first_move;
    this->last_position = std::make_pair(last_x,last_y);

}
//======================================================================================================================
King::King(const King &k) {

    this->name = "KING";
    this->x  = k.x;
    this->y  = k.y;
    this->alive = true;
    this->c = k.c;
    this->first_move = k.first_move;
    this->last_position = k.last_position;
    this->prev_first_move = k.prev_first_move;
}
//======================================================================================================================
King::King() {

    this->name = "KING";
    this->alive = true;
    this->first_move = false;
}
//======================================================================================================================
King& King::operator=(const King &k) {

    this->name = "KING";
    this->x  = k.x;
    this->y  = k.y;
    this->alive = true;
    this->c = k.c;
    this->first_move = k.first_move;
    this->last_position = k.last_position;
    this->prev_first_move = k.prev_first_move;

    return *this;
}
//======================================================================================================================
bool King::setPosition(const unsigned &x, const unsigned &y) {

    if( x > 8 || x < 1 || y > 8 || y < 1 )
        return 0;


    if( abs((int)(this->x) - (int)(x)) <= 1 && abs((int)(this->y) - (int)(y)) <= 1 )
    {
        this->last_position = std::make_pair(this->x, this->y);
        this->prev_first_move = this->first_move;

        this->x = x;
        this->y = y;

        this->first_move = true;

        return 1;
    }

    if( !this->first_move )
    {
        if ( abs((int)(this->x) - (int)(x)) == 2 && abs((int)(this->y) - (int)(y)) == 0 )
        {
            this->prev_first_move = this->first_move;

            this->first_move = true;

            this->last_position = std::make_pair(this->x, this->y);

            this->x = x;
            this->y = y;

            return 1;
        }
    }

    return 0;
}

//======================================================================================================================
bool King::threatenPosition(unsigned x, unsigned y) {

    if( abs((int)(this->x) - (int)(x)) <= 1 && abs((int)(this->y) - (int)(y)) <= 1 )
        return true;

    return false;
}
//======================================================================================================================
bool King::wasMoved() const {

    return this->first_move;
}
//======================================================================================================================
std::set<std::pair<std::string, unsigned >>    King::calculateAllPossibleMoves  ()
{
    std::set<std::pair<std::string, unsigned >> all_moves;

    if ( ((this->x - 1) >= 1 && (this->x - 1) <= 8 ) &&  ((this->y ) >= 1 && (this->y) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1), this->y));

    if ( ((this->x + 1) >= 1 && (this->x + 1) <= 8 ) &&  ((this->y ) >= 1 && (this->y) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x + 1), this->y));

    if ( ((this->x ) >= 1 && (this->x ) <= 8 ) &&  ((this->y - 1) >= 1 && (this->y - 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x ), this->y - 1));

    if ( ((this->x ) >= 1 && (this->x ) <= 8 ) &&  ((this->y + 1) >= 1 && (this->y + 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x) , this->y + 1));

    if ( ((this->x - 1) >= 1 && (this->x - 1 ) <= 8 ) &&  ((this->y - 1) >= 1 && (this->y - 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1 ), this->y - 1));

    if ( ((this->x + 1) >= 1 && (this->x + 1 ) <= 8 ) &&  ((this->y + 1) >= 1 && (this->y + 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x + 1) , this->y + 1));

    if ( ((this->x + 1 ) >= 1 && (this->x + 1) <= 8 ) &&  ((this->y - 1) >= 1 && (this->y - 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x + 1), this->y - 1));

    if ( ((this->x - 1 ) >= 1 && (this->x - 1) <= 8 ) &&  ((this->y + 1) >= 1 && (this->y + 1) <= 8 ) )
        all_moves.emplace(std::make_pair(this->unsignedXToString(this->x - 1), this->y + 1));

    return all_moves;
}
//======================================================================================================================
void King::undoMove() {

    this->x          = this->last_position.first;
    this->y          = this->last_position.second;
    this->first_move = this->prev_first_move;
}
//======================================================================================================================
std::string King::saveFigure() {

    std::string figure_data;

    figure_data+=std::to_string(this->y) + " ";
    figure_data+=std::to_string(this->x) + " ";
    figure_data+=std::to_string(this->alive) + " ";
    figure_data+=std::to_string(this->last_position.first) + " ";
    figure_data+= std::to_string(this->last_position.second) + " ";
    figure_data+=std::to_string(this->first_move) + " ";
    figure_data+= std::to_string(this->prev_first_move);

    return figure_data;
}
//======================================================================================================================
King* King::Copy() {
    return new King(*this);
}