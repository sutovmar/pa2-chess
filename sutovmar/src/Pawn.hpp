#pragma once
#include "Figure.hpp"

class Pawn : public Figure
{
    public:

    //==================================================================================================================
    /**
    * Empty constructor
    */
        Pawn                                                                        ();
    //==================================================================================================================
    /**
     * Constructor for initialization of chessboard
     * @param x - starting X position
     * @param y - starting Y position
     * @param color - color of player
     */
        Pawn                                                                        (   const unsigned & x,
                                                                                        const unsigned & y,
                                                                                        const COLOR & color );
    //==================================================================================================================
    /**
     * Copy constructor
     * @param p - object from copy
     */
        Pawn                                                                        ( const Pawn & p );
    //==================================================================================================================
    /**
      * Constructor for recreating board from loaded file
     * @param c - color of player
     * @param x - saved X position of figure
     * @param y - saved Y position of figure
     * @param last_x - saved previous X position of figure
     * @param last_y - saved previous Y position of figure
     * @param alive - saved value if figure was alive
     * @param double_move - saved value if pawn performed his double move
     * @param first_move - saved value if pawn performed his first move
     * @param prev_double_move - previous value if pawn performed his double move for undo function
     * @param prev_first_move - previous value if pawn performed his first move for undo function
     */
        Pawn                                                                        (   const COLOR & c,
                                                                                        const unsigned & x,
                                                                                        const unsigned & y,
                                                                                        const unsigned & last_x,
                                                                                        const unsigned & last_y ,
                                                                                        const bool & alive,
                                                                                        const bool & double_move,
                                                                                        const bool & first_move,
                                                                                        const bool & prev_double_move,
                                                                                        const bool &  prev_first_move );
    //==================================================================================================================
    /**
    * Overloaded operator =
    * @param p - object to copy from
    * @return - copied object
    */
        Pawn& operator=                                                             ( const Pawn & p );
    //==================================================================================================================
        bool setPosition                                                            (   const unsigned & x,
                                                                                        const unsigned & y ) override;
    //==================================================================================================================
        bool threatenPosition                                                       (   unsigned x,
                                                                                        unsigned y)  override;
    //==================================================================================================================
    /**
     * Return previous pair of X position and Y position
     * @return - pair of X position and Y position
     */
        std::pair<unsigned,unsigned> getLastPosition                                () const;
    //==================================================================================================================
        std::set<std::pair<std::string, unsigned >>    calculateAllPossibleMoves   () override;
    //==================================================================================================================
    /**
     * Check if last move was double
     * @return
     */
    bool    wasLastMoveDouble                                                       () const;
    //==================================================================================================================
    void    undoMove                                                                () override;
    //==================================================================================================================
    std::string saveFigure                        () override ;
    //==================================================================================================================
    Pawn* Copy() override ;
    //==================================================================================================================

    private:

        bool double_move;
        bool first_move;

        bool prev_double_move;
        bool prev_first_move;

    //==================================================================================================================
};