#include "Rook.hpp"

//======================================================================================================================
Rook::Rook(const unsigned &x, const unsigned &y, const COLOR & color) {

    this->x = x;
    this->y = y;

    this->name = "ROOK";
    this->alive = true;

    this->first_move = false;

    this->c = color;
}
//======================================================================================================================
Rook::Rook(const COLOR &c, const unsigned &x, const unsigned &y, const unsigned &last_x, const unsigned &last_y,
           const bool &alive, const bool &first_move, const bool &last_first_move) {

    this->x = x;
    this->y = y;

    this->name = "ROOK";
    this->alive = alive;
    this->c = c;

    this->last_position = std::make_pair(last_x, last_y);
    this->first_move = first_move;
    this->last_first_move = last_first_move;

}
//======================================================================================================================
Rook::Rook() {
    this->name = "ROOK";
    this->alive = true;
    this->first_move = false;
}
//======================================================================================================================
Rook::Rook(const Rook &r) {
    this->x  = r.x;
    this->y  = r.y;
    this->name = "ROOK";
    this->alive = true;
    this->c = r.c;
    this->first_move = r.first_move;
    this->last_position = r.last_position;
    this->last_first_move = r.last_first_move;
}
//======================================================================================================================
Rook& Rook::operator=(const Rook &r) {

    this->x  = r.x;
    this->y  = r.y;
    this->name = "ROOK";
    this->alive = true;
    this->c = r.c;
    this->first_move = r.first_move;
    this->last_position = r.last_position;
    this->last_first_move = r.last_first_move;

    return *this;
}
//======================================================================================================================
std::string Rook::saveFigure() {

    std::string figure_data;

    figure_data+=(std::to_string(this->y) + " " + std::to_string(this->x) + " " + std::to_string(this->alive) + " " +  std::to_string(this->last_position.first) + " " + std::to_string(this->last_position.second) + " " + std::to_string(this->first_move) + " " + std::to_string(this->last_first_move));

    return figure_data;
}
//======================================================================================================================
bool Rook::setPosition(const unsigned &x, const unsigned &y) {
    if( x > 8 || x < 1 || y > 8 || y < 1 )
    {
        return 0;
    }

    if( this->x != x && this->y != y )
    {
        return 0;
    }

    this->last_position     = std::make_pair(this->x, this->y);
    this->last_first_move   = this->first_move;

    this->x = x;
    this->y = y;

    if( !this->first_move )
        this->first_move = true;

    return 1;
}
//======================================================================================================================
bool Rook::threatenPosition(unsigned x, unsigned y)  {

    if ( this->x == x || this->y == y )
    {
        return true;
    }

    return false;

}
//======================================================================================================================
bool Rook::wasMoved() const {
    return this->first_move;
}
//======================================================================================================================
std::set<std::pair<std::string , unsigned >> Rook::calculateAllPossibleMoves  ()
{

    std::set<std::pair<std::string, unsigned >> all_moves;

    for( unsigned x_position = this->x-1; x_position >= 1; x_position-- )
    {

        all_moves.emplace(std::make_pair(this->unsignedXToString(x_position),y));

    }

    for( unsigned x_position = this->x+1; x_position <= 8; x_position++ )
    {

        all_moves.emplace(std::make_pair(this->unsignedXToString(x_position),y));

    }

    for( unsigned y_position = this->y+1; y_position <= 8; y_position++ )
    {

        all_moves.emplace(std::make_pair(this->unsignedXToString(x),y_position));
    }

    for( unsigned y_position = this->y-1; y_position >= 8; y_position-- )
    {

        all_moves.emplace(std::make_pair(this->unsignedXToString(x),y_position));
    }

    return all_moves;
}
//======================================================================================================================
void Rook::undoMove() {
    this->x             = this->last_position.first;
    this->y             = this->last_position.second;
    this->first_move    = this->last_first_move;
}
//======================================================================================================================
Rook* Rook::Copy() {
    return new Rook(*this);
}
//======================================================================================================================