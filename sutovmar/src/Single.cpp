#include "Single.hpp"

//==================================================================================================================
Single::Single() {

    this->startGame();

    this->initEvaluation();
}
//==================================================================================================================
Single::Single(Single &c) {

    for(auto f : c.white.figures )
    {
        this->white.figures.emplace(f.first, f.second->Copy());
    }

    for(auto f : c.white.player_layout )
    {
        this->white.player_layout.emplace(std::make_pair(f.first.first, f.first.second), f.second->Copy());
    }

    for(auto f : c.black.figures )
    {
        this->black.figures.emplace(f.first, f.second->Copy());
    }

    for(auto f : c.black.player_layout )
    {
        this->black.player_layout.emplace(std::make_pair(f.first.first, f.first.second), f.second->Copy());
    }

    this->ai_color = c.ai_color;
    this->board_evaluation = c.board_evaluation;
    this->ai_level = c.ai_level;
    this->player_color = c.player_color;
}
//==================================================================================================================
void Single::initEvaluation() {

    std::vector<int> pawn_position_evaluation_black;
    std::vector<int> knight_position_evaluation_black;
    std::vector<int> bishop_position_evaluation_black;
    std::vector<int> rook_position_evaluation_black;
    std::vector<int> queen_position_evaluation_black;
    std::vector<int> king_position_evaluation_black;

    std::vector<int> pawn_position_evaluation {  0,  0,  0,  0,  0,  0,  0,  0,
                                                50, 50, 50, 50, 50, 50, 50, 50,
                                                10, 10, 20, 30, 30, 20, 10, 10,
                                                5,  5, 10, 25, 25, 10,  5,  5,
                                                0,  0,  0, 20, 20,  0,  0,  0,
                                                5, -5,-10,  0,  0,-10, -5,  5,
                                                5, 10, 10,-20,-20, 10, 10,  5,
                                                0,  0,  0,  0,  0,  0,  0,  0  };

    std::vector<int> knight_position_evaluation { -50,-40,-30,-30,-30,-30,-40,-50,
                                                 -40,-20,  0,  0,  0,  0,-20,-40,
                                                 -30,  0, 10, 15, 15, 10,  0,-30,
                                                 -30,  5, 15, 20, 20, 15,  5,-30,
                                                 -30,  0, 15, 20, 20, 15,  0,-30,
                                                 -30,  5, 10, 15, 15, 10,  5,-30,
                                                 -40,-20,  0,  5,  5,  0,-20,-40,
                                                 -50,-40,-30,-30,-30,-30,-40,-50 };

    std::vector<int> bishop_position_evaluation { 20,-10,-10,-10,-10,-10,-10,-20,
                                                 -10,  0,  0,  0,  0,  0,  0,-10,
                                                 -10,  0,  5, 10, 10,  5,  0,-10,
                                                 -10,  5,  5, 10, 10,  5,  5,-10,
                                                 -10,  0, 10, 10, 10, 10,  0,-10,
                                                 -10, 10, 10, 10, 10, 10, 10,-10,
                                                 -10,  5,  0,  0,  0,  0,  5,-10,
                                                 -20,-10,-10,-10,-10,-10,-10,-20  };

    std::vector<int> rook_position_evaluation {  0,  0,  0,  0,  0,  0,  0,  0,
                                                5, 10, 10, 10, 10, 10, 10,  5,
                                                -5,  0,  0,  0,  0,  0,  0, -5,
                                                -5,  0,  0,  0,  0,  0,  0, -5,
                                                -5,  0,  0,  0,  0,  0,  0, -5,
                                                -5,  0,  0,  0,  0,  0,  0, -5,
                                                -5,  0,  0,  0,  0,  0,  0, -5,
                                                0,  0,  0,  5,  5,  0,  0,  0 };

    std::vector<int> queen_position_evaluation { -20,-10,-10, -5, -5,-10,-10,-20,
                                                 -10,  0,  0,  0,  0,  0,  0,-10,
                                                 -10,  0,  5,  5,  5,  5,  0,-10,
                                                 -5,  0,  5,  5,  5,  5,  0, -5,
                                                 0,  0,  5,  5,  5,  5,  0, -5,
                                                 -10,  5,  5,  5,  5,  5,  0,-10,
                                                 -10,  0,  5,  0,  0,  0,  0,-10,
                                                 -20,-10,-10, -5, -5,-10,-10,-20 };

    std::vector<int> king_position_evaluation { -30,-40,-40,-50,-50,-40,-40,-30,
                                               -30,-40,-40,-50,-50,-40,-40,-30,
                                               -30,-40,-40,-50,-50,-40,-40,-30,
                                               -30,-40,-40,-50,-50,-40,-40,-30,
                                               -20,-30,-30,-40,-40,-30,-30,-20,
                                               -10,-20,-20,-20,-20,-20,-20,-10,
                                               20, 20,  0,  0,  0,  0, 20, 20,
                                               20, 30, 10,  0,  0, 10, 30, 20 };

    for( int y = 7; y >= 0; y--  )
    {
        for( int x = 7; x >= 0; x-- )
        {
            pawn_position_evaluation_black.push_back( pawn_position_evaluation[ y * 8 + x ] );
            knight_position_evaluation_black.push_back( knight_position_evaluation[ y * 8 + x ]);
            bishop_position_evaluation_black.push_back( bishop_position_evaluation[ y * 8 + x ]);
            rook_position_evaluation_black.push_back( rook_position_evaluation[ y*8 + x ]);
            queen_position_evaluation_black.push_back(queen_position_evaluation[ y*8 + x ]);
            king_position_evaluation_black.push_back(king_position_evaluation[ y * 8 + x ]);
        }
    }


    this->board_evaluation.emplace(std::make_pair(WHITE, "PAWN"), pawn_position_evaluation);
    this->board_evaluation.emplace(std::make_pair(WHITE, "KNIGHT"), knight_position_evaluation);
    this->board_evaluation.emplace(std::make_pair(WHITE, "BISHOP"), bishop_position_evaluation);
    this->board_evaluation.emplace(std::make_pair(WHITE, "ROOK"), rook_position_evaluation);
    this->board_evaluation.emplace(std::make_pair(WHITE, "QUEEN"), queen_position_evaluation);
    this->board_evaluation.emplace(std::make_pair(WHITE, "KING"), king_position_evaluation);

    this->board_evaluation.emplace(std::make_pair(BLACK, "PAWN"), pawn_position_evaluation_black);
    this->board_evaluation.emplace(std::make_pair(BLACK, "KNIGHT"), knight_position_evaluation_black);
    this->board_evaluation.emplace(std::make_pair(BLACK, "BISHOP"), bishop_position_evaluation_black);
    this->board_evaluation.emplace(std::make_pair(BLACK, "ROOK"), rook_position_evaluation_black);
    this->board_evaluation.emplace(std::make_pair(BLACK, "QUEEN"), queen_position_evaluation_black);
    this->board_evaluation.emplace(std::make_pair(BLACK, "KING"), king_position_evaluation_black);
}
//==================================================================================================================
void Single::Play() {

    std::cout << "starting game" << std::endl;
    std::string difficulty;

    std::cout << "Please choose AI level - LOW | MEDIUM | HIGH" << std::endl;

    std::cin.clear();

    std::cin >> difficulty;

    std::locale loc;
    std::string difficulty_lower = "";

    for( auto c : difficulty )
    {
        difficulty_lower=difficulty_lower+std::tolower(c,loc);
    }

    while ( std::cin.fail() || (difficulty_lower != "low" && difficulty_lower != "medium" && difficulty_lower != "high") ) {

        std::cout << "Unknown difficulty, please try again" << std::endl;

        std::cin.clear();

        std::cin.ignore(1024, '\n');;

        std::cin >> difficulty;

        difficulty_lower = "";

        for( auto c : difficulty )
        {
            difficulty_lower=difficulty_lower+std::tolower(c,loc);
        }
    }

    if (difficulty_lower == "low")
    {
        this->ai_level = LOW;

    } else if( difficulty_lower == "medium" )
    {
        this->ai_level = MEDIUM;

    } else if( difficulty_lower == "high" )
    {
        this->ai_level = HIGH;
    }

    std::cout << "Please choose color - WHITE | BLACK" << std::endl;

    std::string color;

    std::cin >> color;

    std::string color_lower = "";

    for( auto c : color )
    {
        color_lower=color_lower+std::tolower(c,loc);
    }

    while ( std::cin.fail() || ( color_lower != "white" && color_lower != "black") )
    {
        std::cout << "Unknown color, please try again" << std::endl;

        std::cin.clear();

        std::cin.ignore(1024, '\n');;

        std::cin >> color;

        color_lower = "";

        for( auto c : color )
        {
            color_lower=color_lower+std::tolower(c,loc);
        }
    }

    if( color_lower == "white" )
    {
        this->player_color = WHITE;

        this->ai_color     = COLOR::BLACK;

    } else {

        this->player_color = BLACK;

        this->ai_color     = COLOR::WHITE;
    }

    std::string command;
    COLOR playing_color = WHITE;

    while( true )
    {
        std::cout << "---------------------------------------------" << std::endl;
        this->Print();
        std::cout << "---------------------------------------------" << std::endl;

        if ( playing_color == WHITE )
            std::cout << "White goes: " << std::endl;
        else
            std::cout << "Black goes: " << std::endl;

        if ( this->ai_color == playing_color ) {

            this->MakeAIMove();

            if (this->isCheckMate(playing_color)) {
                break;
            }

            if (this->isCheck(playing_color)) {

                if (playing_color == WHITE)
                    std::cout << "Black king is in check" << std::endl;
                else
                    std::cout << "White king is in check" << std::endl;
            }

        } else {

            std::cout << "Save - [ SAVE GAME ] | Move - [ MAKE MOVE ] | Exit - [EXIT GAME]" << std::endl;

            std::cin >> command;

            int command_out = this->parseCommand(command, playing_color);

            while (command_out == -1 || command_out == 2) {

                if( command_out == -1 )
                    std::cout << "Invalid command" << std::endl;

                std::cout << "Save - [ SAVE GAME ] | Move - [ MAKE MOVE ] | Exit - [EXIT GAME]" << std::endl;

                std::cin.clear();
                std::cin.ignore(1024, '\n');

                std::cin >> command;

                command_out = this->parseCommand(command, playing_color);
            }

            if (command_out == 0) {
                break;
            }
        }

        if ( playing_color == WHITE )

            playing_color = BLACK;
        else
            playing_color = WHITE;
    }

}
//==================================================================================================================
void Single::MakeAIMove() {

    switch ( this->ai_level )
    {
        case LOW:

            if ( !this->RandomMoves() )
            {
                std::cout << "Problem with computer AI" << std::endl;
            }

            break;

        case MEDIUM:

            if ( !this->PositionEvaluation() )
            {
                std::cout << "Problem with computer AI" << std::endl;
            }

            break;

        case HIGH:

            this->Minimax();
            break;
    }


}
//==================================================================================================================
bool Single::RandomMoves() {

    std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves  = this->generateAllMoves(this->ai_color);

    srand(time(NULL));

    unsigned random_figure_move = rand() % ( all_moves.size() + 1 );

    auto figure_move = all_moves[random_figure_move];

    while( !this->validMove( this->ai_color, figure_move.first, figure_move.second.first, figure_move.second.second , true, false) )
    {
        rand();

        srand(time(NULL));

        random_figure_move = rand() % ( all_moves.size() + 1 );

        figure_move = all_moves[random_figure_move];

    }

    std::cout << this->ai_color <<  " MOVED " << figure_move.first << " " << figure_move.second.first << ":" << figure_move.second.second << std::endl;


    return true;
}
//==================================================================================================================
bool Single::PositionEvaluation() {

    std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves  = this->generateAllMoves(this->ai_color);
    std::string best_move_figure = "";
    std::string best_move_x      = "";
    unsigned best_move_y         = 0;
    std::map<std::string, Figure*> figures_layout = ( this->ai_color == WHITE)?(this->white.figures):(this->black.figures);

    int best_eval = (this->ai_color == WHITE) ? INT_MIN : INT_MAX;
    int move_eval;

    for( auto move : all_moves )
    {
        Figure * f = (figures_layout.find(move.second.first))->second;
        this->validMove(this->ai_color, move.second.first, move.second.second.first, move.second.second.second, false,true);

        move_eval = this->EvaluateBoard();

        f->undoMove();

        if ( (this->ai_color == WHITE && move_eval > best_eval) || ( this->ai_color == BLACK && move_eval < best_eval) )
        {
            best_eval = move_eval;
            best_move_figure = move.second.first;
            best_move_x = move.second.second.first;
            best_move_y = move.second.second.second;
        }
    }

    if ( !this->validMove(this->ai_color, best_move_figure, best_move_x, best_move_y, true,true) ) {

        return false;
    }

    std::cout << this->ai_color <<  " MOVED " << best_move_figure << " " << best_move_x << ":" << best_move_y << std::endl;

    return true;
}
//==================================================================================================================
bool Single::Minimax() {

    int depth = this->minimax_depth;

    Single * board = new Single(*this);

    int best_move = (this->ai_color == WHITE) ? INT_MIN: INT_MAX;

    std::string best_figure = "";
    std::string best_x = "";
    unsigned best_y = 0;

    int color = (this->ai_color == WHITE) ? 0: 1;

    std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves = board->generateAllMoves(this->ai_color);

    for( auto move: all_moves )
    {
        Single * tmp = new Single(*board);

        tmp->validMove(this->ai_color, move.second.first, move.second.second.first, move.second.second.second, true, false);

        int value = this->countMinimaxVal(tmp, depth, color);

        if ( (this->ai_color == WHITE && value > best_move) || (this->ai_color == BLACK && value < best_move))
        {
            best_move = value;
            best_figure = move.second.first;
            best_x = move.second.second.first;
            best_y = move.second.second.second;
        }
    }

    if ( !this->validMove(this->ai_color, best_figure, best_x, best_y, true, true) ) {

        return false;
    }

    std::cout << this->ai_color << " MOVED " << best_figure << " " << best_x << " " << best_y << std::endl;

    return true;
}
//==================================================================================================================
int Single::countMinimaxVal( Single * board, int depth, bool color) {

    if ( depth == 0 )
    {
        return board->EvaluateBoard();
    }

    if ( color )
    {
        int best_move = INT_MIN;

        std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves = board->generateAllMoves(WHITE);

        for( auto move: all_moves )
        {
            Single * tmp = new Single(*board);

            tmp->validMove(WHITE, move.second.first, move.second.second.first, move.second.second.second, true,false);

            int this_move = this->countMinimaxVal(tmp, depth-1, !color);

            if ( this_move > best_move )
            {
                best_move = this_move;

            }
        }

        return best_move;

    } else {

        int best_move = INT_MAX;


        std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves = board->generateAllMoves(BLACK);

        for( auto move: all_moves )
        {
            Single * tmp = new Single(*board);

            tmp->validMove(BLACK, move.second.first, move.second.second.first, move.second.second.second, true,false);

            int this_move = this->countMinimaxVal(tmp, depth-1, !color);

            if ( this_move < best_move )
            {
                best_move = this_move;
            }

        }


        return best_move;
    }
}
//==================================================================================================================
std::map<unsigned, std::pair<std::string, std::pair<std::string, unsigned>>> Single::generateAllMoves(const COLOR & color) {

    std::map<unsigned, std::pair<std::string,std::pair<std::string, unsigned>>> all_moves;

    int position = 0;

    std::map<std::string, Figure*> figures = (color == WHITE) ? ( this->white.figures ): (this->black.figures);

    for( auto f : figures)
    {
        if ( (f.second)->isAlive() )
        {
            std::set<std::pair<std::string, unsigned>> figure_positions = (f.second)->calculateAllPossibleMoves();

            for( auto move : figure_positions )
            {
                if ( this->validMove(color, f.first, move.first, move.second, false , false))
                {

                    f.second->undoMove();
                    all_moves.emplace(std::make_pair(position, std::make_pair(f.first, move)));
                    position++;
                    continue;
                }
            }
        }
    }

    return all_moves;
}
//==================================================================================================================
int Single::EvaluateBoard() {

    int evaluation = 0;

    for( auto w : this->white.figures )
    {
        if ( (w.second)->isAlive() )
        {
            std::pair<unsigned, unsigned> figure_position = (w.second)->getCoordinates();

            auto eval  = this->board_evaluation.find(std::make_pair(WHITE, (w.second)->getName()));

            evaluation+=(eval->second[(figure_position.second-1)*8 + figure_position.first - 1]);
        }

    }

    for( auto b : this->black.figures )
    {
        if ( (b.second)->isAlive() )
        {
            std::pair<unsigned, unsigned> figure_position = (b.second)->getCoordinates();

            auto eval  = this->board_evaluation.find(std::make_pair(BLACK, (b.second)->getName()));

            evaluation+=(eval->second[(figure_position.second-1)*8 + figure_position.first - 1]);
        }
    }

    return evaluation;
}
//==================================================================================================================
